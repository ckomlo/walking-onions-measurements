#!/usr/bin/python3
#
# This file is released under the CC0 public domain dedication,
# available at
#
# https://creativecommons.org/share-your-work/public-domain/cc0/


#
# These are copied from table 3.  This function should be called with
# **kwargs.
#

def execute(Nh, Nr, Nc, Br, Pdelta, M, Nh_minus_1, Bid, gamma,
            Bc, Bs, Bidx, Bsnip, Bo, phi, Bauth, Bt, Bv, Be, **junk):

    return {
        'dl_c_vanilla' : Nr * Br * Pdelta,
        'dl_c_t_wo'  : 0,
        'dl_c_sp_wo' : 0,

        'dist_r_vanilla' : (M + 1) * Nr * Br * Pdelta,
        'dist_r_t_wo'  : Nr * (Br + Bo)*Pdelta + phi * Nr * Bauth,
        'dist_r_sp_wo' : Nr * (Br + Bo)*Pdelta + phi * Nr * Bauth,

        'circ_c_vanilla' : Nh_minus_1 * Bid + Nh * (Bc + Bs),
        'circ_c_t_wo' : Nh_minus_1 * (Bidx + Bsnip) + Nh * (Bc + Bs),
        'circ_c_sp_wo' : 2*Bc + Bt + Nh*Bs + Nh_minus_1*(Bsnip + Bv + Be),

        'circ_r_vanilla' : Nh_minus_1**2  * Bid + Nh ** 2 * (Bc + Bs),
        'circ_r_t_wo' : (Nh_minus_1)**2  * (Bidx + Bsnip) + Nh**2 * (Bc + Bs),
        'circ_r_sp_wo' : (2*Nh_minus_1)*(2*Bc + Bt) + Nh**2 * Bs + Nh_minus_1**2 * (Bsnip + Bv + Be),
        }

