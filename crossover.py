#!/usr/bin/python3
#
# This file is released under the CC0 public domain dedication,
# available at
#
# https://creativecommons.org/share-your-work/public-domain/cc0/

from sympy import *
import formulas

# This file uses the formulas from table 3 in the main paper to calculate
# formulas for the points at which walking onions outperforms vanilla
# onion routing.


#
# Define all the symbols from table 2
#

Nr = symbols("N_R")
Nc = symbols("N_C")
Nh = symbols("N_H")
Na = symbols("N_A")

gamma = symbols("gamma")

phi = symbols("\phi")

Br = symbols("B_R")
Bo = symbols("B_O")
Bauth = symbols("B_{AUTH}")
Bsnip = symbols("B_{SNIP}")
Bid = symbols("B_{ID}")
Bidx = symbols("B_{IDX}")
Bt = symbols("B_T")
Bv = symbols("B_V")
Be = symbols("B_E")
Bc = symbols("B_C")
Bs = symbols("B_S")
Bnpd = symbols("B_{NPD}")
Pdelta = symbols("P_\Delta")

# ratio of clients to relays == Nc / Ncr
M = symbols("M")
Nh_minus_1 = symbols("(N_{Hm1})")

#
# Copy formulas from table 3
#

results = formulas.execute(**globals())
for name in results:
    globals()[name] = results[name]

def solve_for_nr(ineq):
    r = simplify(solve(ineq, Nr))
    r = r.subs(Nh_minus_1, Nh - 1)
    r = r.subs(M, Nc / Nr)
    return r

#
# Inequality: walking onions is a bandwith win for clients if ...
#

cost_c_vanilla = dl_c_vanilla + gamma * circ_c_vanilla
cost_c_t_wo = dl_c_t_wo + gamma * circ_c_t_wo

comparison_c_t_wo = Gt(cost_c_vanilla, cost_c_t_wo)

#print(comparison_c_t_wo)
#print()
#print(simplify(comparison_c_t_wo))

print("telescoping is better than vanilla for clients when...")
print(comparison_c_t_wo)
s = solve_for_nr(comparison_c_t_wo)
print("=>")
print(s)
print()
print("LaTeX:")
print(latex(s))
print()

#
# Inequality: walking onions is a bandwidth win for relays if ...
#

cost_r_vanilla = dist_r_vanilla + gamma * M * circ_r_vanilla
cost_r_t_wo = dist_r_t_wo + gamma * M * circ_r_t_wo

comparison_r_t_wo = Gt(cost_r_vanilla, cost_r_t_wo)

print("telescoping is better than vanilla for relays when...")
print(comparison_r_t_wo)
s = solve_for_nr(comparison_r_t_wo)
print("=>")
print(s)
print()
print("LaTeX:")
print(latex(s))
print()

