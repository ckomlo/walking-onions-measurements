# Code to validate derived performance results for Walking Onions

This is a trio of python scripts used to validate performance results presented
in Chelsea Komlo's thesis, "Walking Onions: Scaling Distribution of
Information Safely in Anonymity Networks". These scripts double check the
symbolic computation and to compute crossover points for
bandwidth cost functions.

Note that the simulator used for the version of our work presented at the
[USENIX Security Symposium](https://cs.uwaterloo.ca/~iang/pubs/walkingonions-usenix20.pdf)
is at
<https://git-crysp.uwaterloo.ca/iang/walkingonions>.

## Contents

- formulas.py  -- these are copies of the formulas from table 8.2.

- crossover.py -- uses sympy to double-check that our crossover
       point calculations are correct.

- evaluate.py  -- evaluates our cost formulas for different values,
       and prints the results.




