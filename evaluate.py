#!/usr/bin/python3
#
# This file is released under the CC0 public domain dedication,
# available at
#
# https://creativecommons.org/share-your-work/public-domain/cc0/

import formulas
import math
import os
import sys

def ceil_log_2(x):
    return math.ceil(math.log2(x))

def evaluate(Nr, Nc, merkle=True):

    # values from table 6

    # by measurement
    Pdelta = 0.019
    gamma = float(os.getenv('FORCE_GAMMA', 8.9))

    # by construction
    Nh = 3
    Br = 128
    Bo = 32
    Bid = 66
    Bidx = 8
    Bt = 1
    Bv = 80
    Be = 28
    Bc = 32
    Bs = 64

    if merkle:
        Bauth = 32 * ceil_log_2(Nr)
        phi = 0
    else:
        Bauth = 76
        phi = 1

    Bsnip = Br + Bo + Bauth
    Nh_minus_1 = Nh - 1
    M = float(Nc)/Nr

    v = formulas.execute(**locals())

    cost_c_vanilla = v['dl_c_vanilla'] + gamma * v['circ_c_vanilla']
    cost_c_t_wo = v['dl_c_t_wo'] + gamma * v['circ_c_t_wo']
    cost_c_sp_wo = v['dl_c_sp_wo'] + gamma * v['circ_c_sp_wo']

    cost_r_vanilla = v['dist_r_vanilla'] + gamma * M * v['circ_r_vanilla']
    cost_r_t_wo = v['dist_r_t_wo'] + gamma * M * v['circ_r_t_wo']
    cost_r_sp_wo = v['dist_r_sp_wo'] + gamma * M * v['circ_r_sp_wo']

    return ( (cost_c_vanilla, cost_c_t_wo, cost_c_sp_wo),
             (cost_r_vanilla, cost_r_t_wo, cost_r_sp_wo))

def experiment(Nr, Nc):
    ((ccv, cctwothresh, ccspwothresh), (crv, crtwothresh, crspwothresh)) = evaluate(Nr, Nc, False)

    ((_, cctwomerkle, ccspwomerkle), (_, crtwomerkle, crspwomerkle)) = evaluate(Nr, Nc, True)

    return (Nr, Nc, ccv, crv, cctwothresh, crtwothresh, cctwomerkle, crtwomerkle, ccspwothresh, crspwothresh, ccspwomerkle, crspwomerkle)

def write(res):
    for item in res:
        if type(item) == float:
            item = int(item)
        print("{:10}".format(item), end="\t")
    print()


if len(sys.argv) > 1:
    r = int(sys.argv[1])
    c = int(2500000*r/6500)
    write(experiment(r, c))
    sys.exit(0)

write(("Nr", "Nc", "C(vanilla)", "R(vanilla)", "C(tele_threshold)", "R(tele_threshold)", "C(tele_merkle)", "R(tele_merkle)", "C(sp_threshold)", "R(sp_threshold)", "C(sp_merkle)", "R(sp_merkle)"))
r = 6500
c = 2500000
write(experiment(r, c))
r *= 2
c *= 2
write(experiment(r, c))
r *= 5
c *= 5
write(experiment(r, c))
r *= 10
c *= 10
write(experiment(r, c))

print()

def compare(idx1, idx2):
    r = 65
    c = 25000

    while True:
        t = experiment(r,c)

        if t[idx1] > t[idx2]:
            write(t)
            return

        r *= 1.1
        c *= 1.1

print("client crossover vs tele_threshold")
compare(2, 4)

print("client crossover vs tele_merkle")
compare(2, 6)

print("client crossover vs sp_threshold")
compare(2, 8)

print("client crossover vs sp_merkle")
compare(2, 10)

print("relay crossover vs tele_threshold")
try:
    compare(3, 5)
except OverflowError:
    print("Never happens")

print("relay crossover vs tele_merkle")
try:
    compare(3, 7)
except OverflowError:
    print("Never happens")

print("relay crossover vs sp_threshold")
try:
    compare(3, 9)
except OverflowError:
    print("Never happens")

print("relay crossover vs sp_merkle")
try:
    compare(3, 11)
except OverflowError:
    print("Never happens")

